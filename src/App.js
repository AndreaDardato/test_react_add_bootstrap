import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <div className="container">
        Hello
      </div>
      <button className="btn btn-primary">Funziono anche con bootstrap</button>
    </div>
  );
}

export default App;
